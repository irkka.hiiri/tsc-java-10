package ru.tsc.ichaplygina.taskmanager.controller;

import ru.tsc.ichaplygina.taskmanager.api.ITaskController;
import ru.tsc.ichaplygina.taskmanager.api.ITaskService;
import ru.tsc.ichaplygina.taskmanager.model.Task;
import ru.tsc.ichaplygina.taskmanager.util.TerminalUtil;

public class TaskController implements ITaskController {

    private final ITaskService taskService;

    public TaskController(final ITaskService taskService) {
        this.taskService = taskService;
    }

    @Override
    public void showList() {
        int index = 1;
        for (Task task : taskService.findAll()) {
            System.out.println(index + ". " + task);
            index++;
        }
        if (index == 1) System.out.println("No tasks yet. Type <create task> to add a task.");
        System.out.println();
    }

    @Override
    public void clear() {
        int count = taskService.findAll().size();
        System.out.println("Deleting all tasks...");
        taskService.clear();
        System.out.println(count + " tasks deleted");
        System.out.println();
    }

    @Override
    public void create() {
        System.out.println("Creating a new task.");
        final String name = TerminalUtil.readLine("Task name: ");
        final String description = TerminalUtil.readLine("Task description: ");
        if (taskService.add(name,description) == null) System.out.println("ERROR CREATING TASK!");
        else System.out.println("Done. Type <list tasks> to view all tasks.");
        System.out.println();
    }

}
