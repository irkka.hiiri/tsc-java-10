package ru.tsc.ichaplygina.taskmanager.controller;

import ru.tsc.ichaplygina.taskmanager.api.IProjectController;
import ru.tsc.ichaplygina.taskmanager.api.IProjectService;
import ru.tsc.ichaplygina.taskmanager.model.Project;
import ru.tsc.ichaplygina.taskmanager.util.TerminalUtil;

public class ProjectController implements IProjectController {

    private final IProjectService projectService;

    public ProjectController(final IProjectService projectService) {
        this.projectService = projectService;
    }

    @Override
    public void showList() {
        int index = 1;
        for (Project project : projectService.findAll()) {
            System.out.println(index + ". " + project);
            index++;
        }
        if (index == 1) System.out.println("No projects yet. Type <create project> to add a project.");
        System.out.println();
    }

    @Override
    public void clear() {
        int count = projectService.findAll().size();
        System.out.println("Deleting all projects...");
        projectService.clear();
        System.out.println(count + " projects deleted.");
        System.out.println();
    }

    @Override
    public void create() {
        System.out.println("Creating a new project.");
        final String name = TerminalUtil.readLine("Project name: ");
        final String description = TerminalUtil.readLine("Project description: ");
        if (projectService.add(name,description) == null) System.out.println("ERROR CREATING PROJECT!");
        else System.out.println("Done. Type <list projects> to view all projects.");
        System.out.println();
    }
    
}
