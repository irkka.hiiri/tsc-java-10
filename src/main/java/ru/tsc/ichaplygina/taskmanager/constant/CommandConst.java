package ru.tsc.ichaplygina.taskmanager.constant;

public class CommandConst {

    public static final String CMD_VERSION = "version";

    public static final String CMD_ABOUT = "about";

    public static final String CMD_HELP = "help";

    public static final String CMD_INFO = "info";

    public static final String CMD_EXIT = "exit";

    public static final String CMD_LIST_COMMANDS = "list commands";

    public static final String CMD_LIST_ARGUMENTS = "list arguments";

    public static final String CMD_LIST_TASKS = "list tasks";

    public static final String CMD_CLEAR_TASKS = "clear tasks";

    public static final String CMD_CREATE_TASK = "create task";

    public static final String CMD_LIST_PROJECTS = "list projects";

    public static final String CMD_CLEAR_PROJECTS = "clear projects";

    public static final String CMD_CREATE_PROJECT = "create project";

}
