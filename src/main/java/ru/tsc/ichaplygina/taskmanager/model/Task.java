package ru.tsc.ichaplygina.taskmanager.model;

import ru.tsc.ichaplygina.taskmanager.util.NumberUtil;

public class Task {

    private static final String EMPTY = "";

    private static final String DELIMITER = " : ";

    private static final String PLACEHOLDER = "<empty>";

    private String id;

    private String name;

    private String description;

    public Task() {
        this.id = NumberUtil.generateId();
        this.name = EMPTY;
        this.description = EMPTY;
    }

    public Task(final String name) {
        this.id = NumberUtil.generateId();
        this.name = name != null ? name : EMPTY;
    }

    public Task(final String name, final String description) {
        this.id = NumberUtil.generateId();
        this.name = name != null ? name : EMPTY;
        this.description = description != null ? description : EMPTY;
    }

    public String getId() {
        return this.id;
    }

    public void setId(final String id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(final String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return this.id
                + DELIMITER
                + (!this.name.equals(EMPTY) ? this.name : PLACEHOLDER)
                + DELIMITER
                + (!this.description.equals(EMPTY) ? this.description : PLACEHOLDER);
    }

}
