package ru.tsc.ichaplygina.taskmanager.service;

import ru.tsc.ichaplygina.taskmanager.api.ITaskRepository;
import ru.tsc.ichaplygina.taskmanager.api.ITaskService;
import ru.tsc.ichaplygina.taskmanager.model.Task;

import java.util.List;

public class TaskService implements ITaskService {

    private final ITaskRepository taskRepository;

    public TaskService(final ITaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    @Override
    public List<Task> findAll() {
        return this.taskRepository.findAll();
    }

    @Override
    public Task add(final String name, final String description) {
        if (name == null || name.isEmpty()) return null;
        final Task task = new Task(name, description);
        this.taskRepository.add(task);
        return task;
    }

    @Override
    public void clear() {
        this.taskRepository.clear();
    }

}
