package ru.tsc.ichaplygina.taskmanager.service;

import ru.tsc.ichaplygina.taskmanager.api.IProjectRepository;
import ru.tsc.ichaplygina.taskmanager.api.IProjectService;
import ru.tsc.ichaplygina.taskmanager.model.Project;

import java.util.List;

public class ProjectService implements IProjectService {

    private final IProjectRepository projectRepository;

    public ProjectService(final IProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    @Override
    public List<Project> findAll() {
        return projectRepository.findAll();
    }

    @Override
    public Project add(final String name, final String description) {
        if (name == null || name.isEmpty()) return null;
        final Project project = new Project(name, description);
        this.projectRepository.add(project);
        return project;
    }

    @Override
    public void clear() {
        projectRepository.clear();
    }

}
