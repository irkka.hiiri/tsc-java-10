package ru.tsc.ichaplygina.taskmanager.api;

import ru.tsc.ichaplygina.taskmanager.model.Project;

import java.util.List;

public interface IProjectService {

    List<Project> findAll();

    Project add(final String name, final String description);

    void clear();

}
