package ru.tsc.ichaplygina.taskmanager.api;

public interface ITaskController {

    void showList();

    void clear();

    void create();

}
