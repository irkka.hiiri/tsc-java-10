package ru.tsc.ichaplygina.taskmanager.api;

import ru.tsc.ichaplygina.taskmanager.model.Project;

import java.util.List;

public interface IProjectRepository {

    List<Project> findAll();

    void add(final Project project);

    void remove(final Project project);

    void clear();

}
