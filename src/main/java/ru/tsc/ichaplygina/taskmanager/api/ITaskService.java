package ru.tsc.ichaplygina.taskmanager.api;

import ru.tsc.ichaplygina.taskmanager.model.Task;

import java.util.List;

public interface ITaskService {

    List<Task> findAll();

    Task add(final String name, final String description);

    void clear();

}
