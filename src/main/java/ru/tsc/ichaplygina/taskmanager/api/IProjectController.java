package ru.tsc.ichaplygina.taskmanager.api;

public interface IProjectController {

    void showList();

    void clear();

    void create();

}
