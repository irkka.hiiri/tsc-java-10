package ru.tsc.ichaplygina.taskmanager.api;

public interface ICommandController {

    void showHelp();

    void showCommands();

    void showArguments();

    void showUnknown(String arg);

    void exit();

    void showWelcome();

    void showVersion();

    void showAbout();

    void showSystemInfo();

}
