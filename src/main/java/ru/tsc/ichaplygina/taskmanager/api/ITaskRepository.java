package ru.tsc.ichaplygina.taskmanager.api;

import ru.tsc.ichaplygina.taskmanager.model.Task;

import java.util.List;

public interface ITaskRepository {

    List<Task> findAll();

    void add(final Task task);

    void remove(final Task task);

    void clear();

}
