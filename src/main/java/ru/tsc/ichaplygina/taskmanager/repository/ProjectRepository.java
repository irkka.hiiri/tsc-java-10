package ru.tsc.ichaplygina.taskmanager.repository;

import ru.tsc.ichaplygina.taskmanager.api.IProjectRepository;
import ru.tsc.ichaplygina.taskmanager.model.Project;

import java.util.ArrayList;
import java.util.List;

public class ProjectRepository implements IProjectRepository {

    private final List<Project> list = new ArrayList<>();

    @Override
    public List<Project> findAll() {
        return this.list;
    }

    @Override
    public void add(final Project project) {
        this.list.add(project);
    }

    @Override
    public void remove(final Project project) {
        this.list.remove(project);
    }

    @Override
    public void clear() {
        this.list.clear();
    }

}
