package ru.tsc.ichaplygina.taskmanager.repository;

import ru.tsc.ichaplygina.taskmanager.api.ITaskRepository;
import ru.tsc.ichaplygina.taskmanager.model.Task;

import java.util.ArrayList;
import java.util.List;

public class TaskRepository implements ITaskRepository {

    private final List<Task> list = new ArrayList<>();

    @Override
    public List<Task> findAll() {
        return this.list;
    }

    @Override
    public void add(final Task task) {
        this.list.add(task);
    }

    @Override
    public void remove(final Task task) {
        this.list.remove(task);
    }

    @Override
    public void clear() {
        this.list.clear();
    }

}
