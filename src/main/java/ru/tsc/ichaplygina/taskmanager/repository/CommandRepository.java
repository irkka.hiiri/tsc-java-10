package ru.tsc.ichaplygina.taskmanager.repository;

import ru.tsc.ichaplygina.taskmanager.api.ICommandRepository;
import ru.tsc.ichaplygina.taskmanager.constant.ArgumentConst;
import ru.tsc.ichaplygina.taskmanager.constant.CommandConst;
import ru.tsc.ichaplygina.taskmanager.model.Command;

import java.util.ArrayList;
import java.util.List;

public final class CommandRepository implements ICommandRepository {

    private static final Command ABOUT = new Command(CommandConst.CMD_ABOUT,
            "show developer info",
            ArgumentConst.ARG_ABOUT);

    private static final Command HELP = new Command(CommandConst.CMD_HELP,
            "show this message",
            ArgumentConst.ARG_HELP);

    private static final Command VERSION = new Command(CommandConst.CMD_VERSION,
            "show version info",
            ArgumentConst.ARG_VERSION);

    private static final Command EXIT = new Command(CommandConst.CMD_EXIT,
            "quit");

    private static final Command INFO = new Command(CommandConst.CMD_INFO,
            "show system info",
            ArgumentConst.ARG_INFO);

    private static final Command LIST_COMMANDS = new Command(CommandConst.CMD_LIST_COMMANDS,
            "list available commands");

    private static final Command LIST_ARGUMENTS = new Command(CommandConst.CMD_LIST_ARGUMENTS,
            "list available command-line arguments");

    private static final Command LIST_TASKS = new Command(CommandConst.CMD_LIST_TASKS,
            "show all tasks");

    private static final Command LIST_PROJECTS = new Command(CommandConst.CMD_LIST_PROJECTS,
            "show all projects");

    private static final Command CREATE_TASK = new Command(CommandConst.CMD_CREATE_TASK,
            "create a new task");

    private static final Command CREATE_PROJECT = new Command(CommandConst.CMD_CREATE_PROJECT,
            "create a new project");

    private static final Command CLEAR_TASKS = new Command(CommandConst.CMD_CLEAR_TASKS,
            "delete all tasks");

    private static final Command CLEAR_PROJECTS = new Command(CommandConst.CMD_CLEAR_PROJECTS,
            "delete all projects");

    private static final Command[] COMMANDS = {
            LIST_TASKS, CREATE_TASK, CLEAR_TASKS,
            LIST_PROJECTS, CREATE_PROJECT, CLEAR_PROJECTS,
            INFO, ABOUT, VERSION, HELP, LIST_COMMANDS, LIST_ARGUMENTS, EXIT
    };

    private static final String[] ARGUMENTS = initArguments();

    private static final String[] TERMINAL_COMMANDS = initTerminalCommands();

    private static String[] initArguments() {
        List<String> arrayList = new ArrayList<>();
        for (Command command : COMMANDS) {
            if (command.getArg() != null && !command.getArg().isEmpty())
                arrayList.add(command.getArg());
        }
        return arrayList.toArray(new String[arrayList.size()]);
    }

    private static String[] initTerminalCommands() {
        List<String> arrayList = new ArrayList<>();
        for (Command command : COMMANDS) {
            if (command.getName() != null && !command.getName().isEmpty())
                arrayList.add(command.getName());
        }
        return arrayList.toArray(new String[arrayList.size()]);
    }

    @Override
    public Command[] getAllCommands() {
        return COMMANDS;
    }

    @Override
    public String[] getTerminalCommands() {
        return TERMINAL_COMMANDS;
    }

    @Override
    public String[] getArguments() {
        return ARGUMENTS;
    }

}
