package ru.tsc.ichaplygina.taskmanager.bootstrap;

import ru.tsc.ichaplygina.taskmanager.api.*;
import ru.tsc.ichaplygina.taskmanager.constant.ArgumentConst;
import ru.tsc.ichaplygina.taskmanager.constant.CommandConst;
import ru.tsc.ichaplygina.taskmanager.controller.CommandController;
import ru.tsc.ichaplygina.taskmanager.controller.ProjectController;
import ru.tsc.ichaplygina.taskmanager.controller.TaskController;
import ru.tsc.ichaplygina.taskmanager.repository.CommandRepository;
import ru.tsc.ichaplygina.taskmanager.repository.ProjectRepository;
import ru.tsc.ichaplygina.taskmanager.repository.TaskRepository;
import ru.tsc.ichaplygina.taskmanager.service.CommandService;
import ru.tsc.ichaplygina.taskmanager.service.ProjectService;
import ru.tsc.ichaplygina.taskmanager.service.TaskService;
import ru.tsc.ichaplygina.taskmanager.util.TerminalUtil;

public final class Bootstrap {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ICommandController commandController = new CommandController(commandService);

    private final ITaskRepository taskRepository = new TaskRepository();

    private final ITaskService taskService = new TaskService(taskRepository);

    private final ITaskController taskController = new TaskController(taskService);

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final IProjectController projectController = new ProjectController(projectService);

    public void run(final String... args) {
        if (args == null || args.length == 0) processInput();
        else executeCommand(args);
    }

    public void processInput() {
        commandController.showWelcome();
        String command = readCommand();
        while (true) {
            executeCommand(command);
            command = readCommand();
        }
    }

    public void executeCommand(final String command) {
        if (command.isEmpty()) return;
        switch (command) {
            case CommandConst.CMD_LIST_TASKS:
                taskController.showList();
                break;
            case CommandConst.CMD_CREATE_TASK:
                taskController.create();
                break;
            case CommandConst.CMD_CLEAR_TASKS:
                taskController.clear();
                break;
            case CommandConst.CMD_LIST_PROJECTS:
                projectController.showList();
                break;
            case CommandConst.CMD_CREATE_PROJECT:
                projectController.create();
                break;
            case CommandConst.CMD_CLEAR_PROJECTS:
                projectController.clear();
                break;
            case CommandConst.CMD_ABOUT:
                commandController.showAbout();
                break;
            case CommandConst.CMD_VERSION:
                commandController.showVersion();
                break;
            case CommandConst.CMD_HELP:
                commandController.showHelp();
                break;
            case CommandConst.CMD_INFO:
                commandController.showSystemInfo();
                break;
            case CommandConst.CMD_LIST_COMMANDS:
                commandController.showCommands();
                break;
            case CommandConst.CMD_LIST_ARGUMENTS:
                commandController.showArguments();
                break;
            case CommandConst.CMD_EXIT:
                commandController.exit();
            default:
                commandController.showUnknown(command);
        }
    }

    public void executeCommand(final String[] params) {
        if (params == null || params.length == 0) return;
        switch (params[0]) {
            case ArgumentConst.ARG_ABOUT:
                commandController.showAbout();
                break;
            case ArgumentConst.ARG_VERSION:
                commandController.showVersion();
                break;
            case ArgumentConst.ARG_HELP:
                commandController.showHelp();
                break;
            case ArgumentConst.ARG_INFO:
                commandController.showSystemInfo();
                break;
            default:
                commandController.showUnknown(params[0]);
        }
    }

    public String readCommand() {
        return TerminalUtil.readLine(TerminalUtil.COMMAND_PROMPT);
    }

}
